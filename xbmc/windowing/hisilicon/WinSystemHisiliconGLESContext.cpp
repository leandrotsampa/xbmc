/*
 *  Copyright (C) 2005-2018 Team Kodi
 *  This file is part of Kodi - https://kodi.tv
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 *  See LICENSES/README.md for more information.
 */

#include "WinSystemHisiliconGLESContext.h"
#include "utils/log.h"
#include "threads/SingleLock.h"

std::unique_ptr<CWinSystemBase> CWinSystemBase::CreateWinSystem() {
	std::unique_ptr<CWinSystemBase> winSystem(new CWinSystemHisiliconGLESContext());
	return winSystem;
}

bool CWinSystemHisiliconGLESContext::InitWindowSystem() {
	if (!CWinSystemHisilicon::InitWindowSystem())
		return false;

	if (!m_pGLContext.CreateDisplay(m_nativeDisplay))
		return false;

	if (!m_pGLContext.InitializeDisplay(EGL_OPENGL_ES_API))
		return false;

	if (!m_pGLContext.ChooseConfig(EGL_OPENGL_ES2_BIT))
		return false;

	CEGLAttributesVec contextAttribs;
	contextAttribs.Add({{EGL_CONTEXT_CLIENT_VERSION, 2}});
	return m_pGLContext.CreateContext(contextAttribs);
}

bool CWinSystemHisiliconGLESContext::CreateNewWindow(const std::string& name, bool fullScreen, RESOLUTION_INFO& res) {
	m_pGLContext.DestroySurface();
	if (!CWinSystemHisilicon::DestroyWindow())
		return false;

	if (!CWinSystemHisilicon::CreateNewWindow(name, fullScreen, res))
		return false;

	if (!m_pGLContext.CreateSurface(m_nativeWindow))
		return false;

	if (!m_pGLContext.BindContext())
		return false;

	if (!m_delayDispReset) {
		CSingleLock lock(m_resourceSection);
		// tell any shared resources
		for (std::vector<IDispResource *>::iterator i = m_resources.begin(); i != m_resources.end(); ++i)
			(*i)->OnResetDisplay();
	}

	return true;
}

bool CWinSystemHisiliconGLESContext::ResizeWindow(int newWidth, int newHeight, int newLeft, int newTop) {
	CRenderSystemGLES::ResetRenderSystem(newWidth, newHeight);
	return true;
}

bool CWinSystemHisiliconGLESContext::SetFullScreen(bool fullScreen, RESOLUTION_INFO& res, bool blankOtherDisplays) {
	CreateNewWindow("", fullScreen, res);
	CRenderSystemGLES::ResetRenderSystem(res.iWidth, res.iHeight);
	return true;
}

void CWinSystemHisiliconGLESContext::SetVSyncImpl(bool enable) {
	if (!m_pGLContext.SetVSync(enable))
		CLog::Log(LOGERROR, "%s -> Could not set egl vsync.", __FUNCTION__);
}

void CWinSystemHisiliconGLESContext::PresentRenderImpl(bool rendered) {
	if (m_delayDispReset && m_dispResetTimer.IsTimePast()) {
		m_delayDispReset = false;
		CSingleLock lock(m_resourceSection);
		// tell any shared resources
		for (std::vector<IDispResource *>::iterator i = m_resources.begin(); i != m_resources.end(); ++i)
			(*i)->OnResetDisplay();
	}

	if (!rendered)
		return;

	// Ignore errors - eglSwapBuffers() sometimes fails during modeswaps on Hisilicon,
	// there is probably nothing we can do about it
	m_pGLContext.TrySwapBuffers();
}

EGLDisplay CWinSystemHisiliconGLESContext::GetEGLDisplay() const {
	return m_pGLContext.GetEGLDisplay();
}

EGLSurface CWinSystemHisiliconGLESContext::GetEGLSurface() const {
	return m_pGLContext.GetEGLSurface();
}

EGLContext CWinSystemHisiliconGLESContext::GetEGLContext() const {
	return m_pGLContext.GetEGLContext();
}

EGLConfig  CWinSystemHisiliconGLESContext::GetEGLConfig() const {
	return m_pGLContext.GetEGLConfig();
}

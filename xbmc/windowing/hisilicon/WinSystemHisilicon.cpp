/*
 *  Copyright (C) 2005-2018 Team Kodi
 *  This file is part of Kodi - https://kodi.tv
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 *  See LICENSES/README.md for more information.
 */

#include "WinSystemHisilicon.h"

#include <string.h>
#include <float.h>

#include "ServiceBroker.h"
#include "cores/RetroPlayer/rendering/VideoRenderers/RPRendererOpenGLES.h"
#include "cores/VideoPlayer/DVDCodecs/Video/DVDVideoCodecHisilicon.h"
#include "cores/VideoPlayer/VideoRenderers/LinuxRendererGLES.h"
#include "cores/VideoPlayer/VideoRenderers/HwDecRender/RendererHisilicon.h"
// AESink Factory
#include "cores/AudioEngine/AESinkFactory.h"
#include "cores/AudioEngine/Sinks/AESinkALSA.h"
#include "windowing/GraphicContext.h"
#include "windowing/Resolution.h"
#include "platform/linux/powermanagement/LinuxPowerSyscall.h"
#include "settings/DisplaySettings.h"
#include "settings/Settings.h"
#include "settings/SettingsComponent.h"
#include "guilib/DispResource.h"
#include "utils/log.h"
#include "utils/SysfsUtils.h"
#include "threads/SingleLock.h"

#include <linux/fb.h>
#include <sys/ioctl.h>

#include <EGL/egl.h>

using namespace KODI;

/** Hisilicon VirtualScreen Display **/
typedef struct hiRECT_S
{
	int s32X;
	int s32Y;
	int s32Width;
	int s32Height;
} HI_RECT_S;

typedef struct hiDISP_VIRTSCREEN_S
{
	int enDisp;
	HI_RECT_S stVirtScreen;
} DISP_VIRTSCREEN_S;

/* Get from file: /proc/msp/module */
#define HI_DISP                 0x00000022
/* Get from file: drv_disp_ioctl.h */
#define IOC_DISP_SET_VIRTSCREEN 12
#define CMD_DISP_SET_VIRTSCREEN _IOW(HI_DISP, IOC_DISP_SET_VIRTSCREEN, DISP_VIRTSCREEN_S)
/** End **/

CWinSystemHisilicon::CWinSystemHisilicon() : m_libinput(new CLibInputHandler) {
	const char *env_framebuffer = getenv("FRAMEBUFFER");

	if (env_framebuffer) {
		std::string framebuffer(env_framebuffer);
		std::string::size_type start = framebuffer.find("fb");
		m_framebuffer_name = framebuffer.substr(start);
	} else {
		m_framebuffer_name = "fb0";
	}

	m_nativeDisplay = EGL_NO_DISPLAY;
	m_nativeWindow = static_cast<EGLNativeWindowType>(NULL);

	m_displayWidth = 0;
	m_displayHeight = 0;

	m_stereo_mode = RENDER_STEREO_MODE_OFF;
	m_delayDispReset = false;

	// Register sink
	AE::CAESinkFactory::ClearSinks();
	CAESinkALSA::Register();
	CLinuxPowerSyscall::Register();
	m_libinput->Start();
}

CWinSystemHisilicon::~CWinSystemHisilicon() {
	if (m_nativeWindow)
		m_nativeWindow = static_cast<EGLNativeWindowType>(NULL);
}

bool CWinSystemHisilicon::InitWindowSystem() {
	m_nativeDisplay = EGL_DEFAULT_DISPLAY;

	CDVDVideoCodecHisilicon::Register();
	CLinuxRendererGLES::Register();
	CRendererHisilicon::Register();

	return CWinSystemBase::InitWindowSystem();
}

bool CWinSystemHisilicon::DestroyWindowSystem() {
	return true;
}

bool CWinSystemHisilicon::CreateNewWindow(const std::string& name, bool fullScreen, RESOLUTION_INFO& res) {
	RESOLUTION_INFO current_resolution;
	current_resolution.iWidth = current_resolution.iHeight = 0;
	RENDER_STEREO_MODE stereo_mode = CServiceBroker::GetWinSystem()->GetGfxContext().GetStereoMode();

	m_nWidth        = res.iWidth;
	m_nHeight       = res.iHeight;
	m_displayWidth  = res.iScreenWidth;
	m_displayHeight = res.iScreenHeight;
	m_fRefreshRate  = res.fRefreshRate;

	if ((m_bWindowCreated && GetNativeResolution(&current_resolution)) &&
		current_resolution.iWidth == res.iWidth && current_resolution.iHeight == res.iHeight &&
		current_resolution.iScreenWidth == res.iScreenWidth && current_resolution.iScreenHeight == res.iScreenHeight &&
		m_bFullScreen == fullScreen && current_resolution.fRefreshRate == res.fRefreshRate &&
		(current_resolution.dwFlags & D3DPRESENTFLAG_MODEMASK) == (res.dwFlags & D3DPRESENTFLAG_MODEMASK) &&
		m_stereo_mode == stereo_mode)
	{
		CLog::Log(LOGDEBUG, "%s -> No need to create a new window.", __FUNCTION__);
		return true;
	}

	int delay = CServiceBroker::GetSettingsComponent()->GetSettings()->GetInt("videoscreen.delayrefreshchange");
	if (delay > 0) {
		m_delayDispReset = true;
		m_dispResetTimer.Set(delay * 100);
	}

	{
		CSingleLock lock(m_resourceSection);
		for (std::vector<IDispResource *>::iterator i = m_resources.begin(); i != m_resources.end(); ++i) {
			(*i)->OnLostDisplay();
		}
	}

	m_stereo_mode = stereo_mode;
	m_bFullScreen = fullScreen;

	if (!m_delayDispReset) {
		CSingleLock lock(m_resourceSection);
		// tell any shared resources
		for (std::vector<IDispResource *>::iterator i = m_resources.begin(); i != m_resources.end(); ++i) {
			(*i)->OnResetDisplay();
		}
	}

	return SetDisplayResolution(res.strId.c_str());
}

bool CWinSystemHisilicon::DestroyWindow() {
	m_nativeWindow = static_cast<EGLNativeWindowType>(NULL);
	return true;
}

void CWinSystemHisilicon::UpdateResolutions() {
	CWinSystemBase::UpdateResolutions();

	RESOLUTION_INFO resDesktop, curDisplay;
	std::vector<RESOLUTION_INFO> resolutions;

	if (!ProbeResolutions(resolutions) || resolutions.empty()) {
		CLog::Log(LOGWARNING, "%s -> ProbeResolutions failed.", __FUNCTION__);
	}

	/* ProbeResolutions includes already all resolutions.
	* Only get desktop resolution so we can replace xbmc's desktop res
	*/
	if (GetNativeResolution(&curDisplay)) {
		resDesktop = curDisplay;
	}

	RESOLUTION ResDesktop = RES_INVALID;
	RESOLUTION res_index  = RES_DESKTOP;

	for (size_t i = 0; i < resolutions.size(); i++) {
		// if this is a new setting,
		// create a new empty setting to fill in.
		if ((int)CDisplaySettings::GetInstance().ResolutionInfoSize() <= res_index) {
			RESOLUTION_INFO res;
			CDisplaySettings::GetInstance().AddResolutionInfo(res);
		}

		CServiceBroker::GetWinSystem()->GetGfxContext().ResetOverscan(resolutions[i]);
		CDisplaySettings::GetInstance().GetResolutionInfo(res_index) = resolutions[i];

		CLog::Log(LOGNOTICE, "Found resolution %d x %d with %d x %d%s @ %f Hz\n",
			resolutions[i].iWidth,
			resolutions[i].iHeight,
			resolutions[i].iScreenWidth,
			resolutions[i].iScreenHeight,
			resolutions[i].dwFlags & D3DPRESENTFLAG_INTERLACED ? "i" : "",
			resolutions[i].fRefreshRate);

		if (resDesktop.iWidth == resolutions[i].iWidth &&
			resDesktop.iHeight == resolutions[i].iHeight &&
			resDesktop.iScreenWidth == resolutions[i].iScreenWidth &&
			resDesktop.iScreenHeight == resolutions[i].iScreenHeight &&
			(resDesktop.dwFlags & D3DPRESENTFLAG_MODEMASK) == (resolutions[i].dwFlags & D3DPRESENTFLAG_MODEMASK) &&
			fabs(resDesktop.fRefreshRate - resolutions[i].fRefreshRate) < FLT_EPSILON)
		{
			ResDesktop = res_index;
		}

		res_index = (RESOLUTION)((int)res_index + 1);
	}

	// set RES_DESKTOP
	if (ResDesktop != RES_INVALID) {
		CLog::Log(LOGNOTICE, "Found (%dx%d%s@%f) at %d, setting to RES_DESKTOP at %d",
			resDesktop.iWidth, resDesktop.iHeight,
			resDesktop.dwFlags & D3DPRESENTFLAG_INTERLACED ? "i" : "",
			resDesktop.fRefreshRate,
			(int)ResDesktop, (int)RES_DESKTOP);

		CDisplaySettings::GetInstance().GetResolutionInfo(RES_DESKTOP) = CDisplaySettings::GetInstance().GetResolutionInfo(ResDesktop);
	}
}

bool CWinSystemHisilicon::Hide() {
	return false;
}

bool CWinSystemHisilicon::Show(bool show) {
	return true;
}

void CWinSystemHisilicon::Register(IDispResource *resource) {
  CSingleLock lock(m_resourceSection);
  m_resources.push_back(resource);
}

void CWinSystemHisilicon::Unregister(IDispResource *resource) {
	CSingleLock lock(m_resourceSection);
	std::vector<IDispResource*>::iterator i = find(m_resources.begin(), m_resources.end(), resource);
	if (i != m_resources.end())
		m_resources.erase(i);
}

bool CWinSystemHisilicon::ProbeResolutions(std::vector<RESOLUTION_INFO> &resolutions) {
	std::string valstr;
	SysfsUtils::GetString("/proc/stb/video/videomode_choices", valstr);
	std::vector<std::string> probe_str = StringUtils::Split(valstr, " ");

	resolutions.clear();
	RESOLUTION_INFO res;
	for (std::vector<std::string>::const_iterator i = probe_str.begin(); i != probe_str.end(); ++i)
	{
		if(Str2Resolution(i->c_str(), &res))
			resolutions.push_back(res);
	}

	return resolutions.size() > 0;
}

bool CWinSystemHisilicon::GetNativeResolution(RESOLUTION_INFO *res) const {
	std::string mode;
	SysfsUtils::GetString("/proc/stb/video/videomode", mode);
	return Str2Resolution(mode.c_str(), res);
}

bool CWinSystemHisilicon::SetDisplayResolution(const char *resolution) {
	RESOLUTION_INFO res;
	if (!Str2Resolution(resolution, &res)) {
		CLog::Log(LOGERROR, "%s -> Failed to set resolution mode '%s'.", __FUNCTION__, resolution);
		return false;
	}
	return (SysfsUtils::SetString("/proc/stb/video/videomode", resolution) == 0 && SetFramebufferResolution(res.iWidth, res.iHeight));
}

bool CWinSystemHisilicon::SetFramebufferResolution(int width, int height) const {
	// Hisilicon FB only supports 1920x1080
	if (!(width > 1920 || height > 1080))
	{
		int fd;
		std::string m_fb = "/dev/" + m_framebuffer_name;

		if ((fd = open(m_fb.c_str(), O_RDWR | O_CLOEXEC)) >= 0)
		{
			struct fb_var_screeninfo vinfo;
			if (ioctl(fd, FBIOGET_VSCREENINFO, &vinfo) == 0)
			{
				vinfo.xres           = width;
				vinfo.yres           = height;
				vinfo.xres_virtual   = width;
				vinfo.yres_virtual   = height * 2;
				vinfo.bits_per_pixel = 32;
				vinfo.activate       = FB_ACTIVATE_ALL;

				if (ioctl(fd, FBIOPUT_VSCREENINFO, &vinfo) != 0)
					CLog::Log(LOGERROR, "%s -> Failed to set FrameBuffer width(%d) height(%d).", __FUNCTION__, width, height);
				else /* Set Hisilicon VirtualScreen Resolution */
				{
					int display;
					if ((display = open("/dev/hi_disp", O_RDWR | O_NONBLOCK)) >= 0)
					{
						DISP_VIRTSCREEN_S virtscreen;
						virtscreen.enDisp 				  = 1; /* HI_UNF_DISPLAY1 */
						virtscreen.stVirtScreen.s32X      = 0;
						virtscreen.stVirtScreen.s32Y      = 0;
						virtscreen.stVirtScreen.s32Height = height;
						virtscreen.stVirtScreen.s32Width  = width;

						if (ioctl(display, CMD_DISP_SET_VIRTSCREEN, &virtscreen) != 0)
							CLog::Log(LOGERROR, "%s -> Failed to set VirtualScreen width(%d) height(%d).", __FUNCTION__, width, height);
						else
						{
							close(display);
							close(fd);
							return true;
						}
					}
					else
						CLog::Log(LOGERROR, "%s -> Failed to open device: /dev/hi_disp", __FUNCTION__);
				}
			}
			else
				CLog::Log(LOGERROR, "%s -> Failed to get FrameBuffer info.", __FUNCTION__);

			close(fd);
		}
		else
			CLog::Log(LOGERROR, "%s -> Failed to open FrameBuffer(%s).", __FUNCTION__, m_fb.c_str());
	}
	return false;
}

bool CWinSystemHisilicon::Str2Resolution(const char *mode, RESOLUTION_INFO *res) const {
	if (!res)
		return false;

	res->iWidth = 0;
	res->iHeight = 0;
	res->iScreenWidth = 0;
	res->iScreenHeight = 0;

	if (!mode)
		return false;

	std::string fromMode = mode;
	StringUtils::Trim(fromMode);

	const char types[] = { 'p', 'i', 'x' };
	for (unsigned int i = 0; i < 3; i++) {
		std::size_t pos = fromMode.find(types[i]);
		if (pos == 0 || pos == std::string::npos)
			continue;

		std::string t1 = fromMode.substr(0, pos);
		if (!StringUtils::IsInteger(t1))
			return false;

		std::string t2 = fromMode.substr(pos + 1);
		if (types[i] == 'x') {
			if (StringUtils::IsInteger(t2)) {
				res->iScreenWidth = StringUtils::ReturnDigits(t1);
				res->iScreenHeight = StringUtils::ReturnDigits(t2);
			}
		} else {
			res->iScreenHeight = StringUtils::ReturnDigits(t1);

			if (t2.empty())
				res->fRefreshRate = 60;
			else if (StringUtils::IsInteger(t2))
				res->fRefreshRate = StringUtils::ReturnDigits(t2);

			if ((int)res->fRefreshRate == 23)
				res->fRefreshRate = 23.976;
			else if ((int)res->fRefreshRate == 29)
				res->fRefreshRate = 29.970;
			else if ((int)res->fRefreshRate == 59)
				res->fRefreshRate = 59.940;

			if (types[i] == 'p')
				res->dwFlags = D3DPRESENTFLAG_PROGRESSIVE;
			else
				res->dwFlags = D3DPRESENTFLAG_INTERLACED;

			if (res->iScreenHeight == 480)
				res->iScreenWidth = 640;
			else if (res->iScreenHeight == 576)
				res->iScreenWidth = 720;
			else if (res->iScreenHeight == 720)
				res->iScreenWidth = 1280;
			else if (res->iScreenHeight == 1080)
				res->iScreenWidth = 1920;
			else if (res->iScreenHeight == 2160)
				res->iScreenWidth = 3840;

			break;
		}
	}

	if (res->iScreenHeight > 1080) {
		res->iWidth = 1920;
		res->iHeight = 1080;
	} else {
		res->iWidth = res->iScreenWidth;
		res->iHeight = res->iScreenHeight;
	}

	//res->iScreen       = 0;
	res->bFullScreen   = true;
	res->iSubtitles    = (int)(0.965 * res->iHeight);
	res->fPixelRatio   = 1.0f;
	res->strId         = fromMode;
	res->strMode       = StringUtils::Format("%dx%d @ %.2f%s - Full Screen", res->iScreenWidth, res->iScreenHeight, res->fRefreshRate,
	res->dwFlags & D3DPRESENTFLAG_INTERLACED ? "i" : "");

	return res->iWidth > 0 && res->iHeight> 0;
}

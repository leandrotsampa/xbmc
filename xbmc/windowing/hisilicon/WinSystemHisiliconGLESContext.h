/*
 *  Copyright (C) 2005-2018 Team Kodi
 *  This file is part of Kodi - https://kodi.tv
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 *  See LICENSES/README.md for more information.
 */

#pragma once

#include "utils/EGLUtils.h"
#include "rendering/gles/RenderSystemGLES.h"
#include "utils/GlobalsHandling.h"
#include "WinSystemHisilicon.h"

class CWinSystemHisiliconGLESContext : public CWinSystemHisilicon, public CRenderSystemGLES {
	public:
		CWinSystemHisiliconGLESContext() = default;
		virtual ~CWinSystemHisiliconGLESContext() = default;

		// Implementation of CWinSystemBase via CWinSystemHisilicon
		CRenderSystemBase *GetRenderSystem() override { return this; }
		bool InitWindowSystem() override;
		bool CreateNewWindow(const std::string& name, bool fullScreen, RESOLUTION_INFO& res) override;
		bool ResizeWindow(int newWidth, int newHeight, int newLeft, int newTop) override;
		bool SetFullScreen(bool fullScreen, RESOLUTION_INFO& res, bool blankOtherDisplays) override;

		EGLDisplay GetEGLDisplay() const;
		EGLSurface GetEGLSurface() const;
		EGLContext GetEGLContext() const;
		EGLConfig  GetEGLConfig() const;

	protected:
		void SetVSyncImpl(bool enable) override;
		void PresentRenderImpl(bool rendered) override;

	private:
		CEGLContextUtils m_pGLContext;
};

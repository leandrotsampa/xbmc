/*
 *  Copyright (C) 2007-2018 Team Kodi
 *  This file is part of Kodi - https://kodi.tv
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 *  See LICENSES/README.md for more information.
 */

#include "RendererHisilicon.h"

#include "cores/VideoPlayer/DVDCodecs/Video/DVDVideoCodecHisilicon.h"
#include "utils/log.h"
#include "utils/SysfsUtils.h"
#include "utils/ScreenshotAML.h"
#include "settings/MediaSettings.h"
#include "cores/VideoPlayer/VideoRenderers/RenderCapture.h"
#include "cores/VideoPlayer/VideoRenderers/RenderFactory.h"
#include "cores/VideoPlayer/VideoRenderers/RenderFlags.h"
#include "settings/AdvancedSettings.h"

CRendererHisilicon::CRendererHisilicon() : m_bConfigured(false) {
	CLog::Log(LOGINFO, "CRendererHisilicon::CRendererHisilicon -> called.");
}

CBaseRenderer* CRendererHisilicon::Create(CVideoBuffer *buffer) {
	/*
	 * CRendererHisilicon is only a bypass, not need VideoBuffer.
	 */
	if (!buffer)
		return new CRendererHisilicon();
	return nullptr;
}

bool CRendererHisilicon::Register() {
	VIDEOPLAYER::CRendererFactory::RegisterRenderer("hisilicon", CRendererHisilicon::Create);
	return true;
}

bool CRendererHisilicon::Configure(const VideoPicture &picture, float fps, unsigned int orientation) {
	m_sourceWidth = picture.iWidth;
	m_sourceHeight = picture.iHeight;
	m_renderOrientation = orientation;

	m_iFlags = GetFlagsChromaPosition(picture.chroma_position) |
	           GetFlagsColorMatrix(picture.color_space, picture.iWidth, picture.iHeight) |
	           GetFlagsColorPrimaries(picture.color_primaries) |
	           GetFlagsStereoMode(picture.stereoMode);

	// Calculate the input frame aspect ratio.
	CalculateFrameAspectRatio(picture.iDisplayWidth, picture.iDisplayHeight);
	SetViewMode(m_videoSettings.m_ViewMode);
	ManageRenderArea();

	m_bConfigured = true;
	return true;
}

CRenderInfo CRendererHisilicon::GetRenderInfo() {
	CRenderInfo info;
	info.max_buffer_size = NUM_BUFFERS;
	info.optimal_buffer_size = NUM_BUFFERS;
	info.opaque_pointer = (void *)this;
	return info;
}

bool CRendererHisilicon::RenderCapture(CRenderCapture* capture) {
	capture->BeginRender();
	capture->EndRender();
	return true;
}

void CRendererHisilicon::AddVideoPicture(const VideoPicture &picture, int index) {
}

void CRendererHisilicon::ReleaseBuffer(int idx) {
}

bool CRendererHisilicon::Supports(ERENDERFEATURE feature) {
	return false;
}

void CRendererHisilicon::RenderUpdate(int index, int index2, bool clear, unsigned int flags, unsigned int alpha) {
	ManageRenderArea();
}

/*
 *  Copyright (C) 2005-2018 Team Kodi
 *  This file is part of Kodi - https://kodi.tv
 *
 *  SPDX-License-Identifier: GPL-2.0-or-later
 *  See LICENSES/README.md for more information.
 */

#pragma once

#include "DVDVideoCodec.h"
#include "DVDStreamInfo.h"
#include "system.h"
#include "threads/CriticalSection.h"

#define PTS_FREQ 90000
#define AUDIODEV "/dev/player/audio0"
#define VIDEODEV "/dev/player/video0"

/** Codec types supported by hisilicon decoder. **/
enum HI_UNF_VCODEC_TYPE_E {
	HI_UNF_VCODEC_TYPE_MPEG2,       /** MPEG2 **/
	HI_UNF_VCODEC_TYPE_MPEG4,       /** MPEG4 DIVX4 DIVX5 **/
	HI_UNF_VCODEC_TYPE_AVS,         /** AVS **/
	HI_UNF_VCODEC_TYPE_H263,        /** H263 **/
	HI_UNF_VCODEC_TYPE_H264,        /** H264 **/
	HI_UNF_VCODEC_TYPE_REAL8,       /** REAL **/
	HI_UNF_VCODEC_TYPE_REAL9,       /** REAL **/
	HI_UNF_VCODEC_TYPE_VC1,         /** VC-1 **/
	HI_UNF_VCODEC_TYPE_VP6,         /** VP6 **/
	HI_UNF_VCODEC_TYPE_VP6F,        /** VP6F **/
	HI_UNF_VCODEC_TYPE_VP6A,        /** VP6A **/
	HI_UNF_VCODEC_TYPE_MJPEG,       /** MJPEG **/
	HI_UNF_VCODEC_TYPE_SORENSON,    /** SORENSON SPARK **/
	HI_UNF_VCODEC_TYPE_DIVX3,       /** DIVX3 **/
	HI_UNF_VCODEC_TYPE_RAW,         /** RAW **/
	HI_UNF_VCODEC_TYPE_JPEG,        /** JPEG, added for VENC **/
	HI_UNF_VCODEC_TYPE_VP8,         /** VP8 **/
	HI_UNF_VCODEC_TYPE_MSMPEG4V1,   /** MS private MPEG4 **/
	HI_UNF_VCODEC_TYPE_MSMPEG4V2,
	HI_UNF_VCODEC_TYPE_MSVIDEO1,    /** MS video **/
	HI_UNF_VCODEC_TYPE_WMV1,
	HI_UNF_VCODEC_TYPE_WMV2,
	HI_UNF_VCODEC_TYPE_RV10,
	HI_UNF_VCODEC_TYPE_RV20,
	HI_UNF_VCODEC_TYPE_SVQ1,        /** Apple video **/
	HI_UNF_VCODEC_TYPE_SVQ3,        /** Apple video **/
	HI_UNF_VCODEC_TYPE_H261,
	HI_UNF_VCODEC_TYPE_VP3,
	HI_UNF_VCODEC_TYPE_VP5,
	HI_UNF_VCODEC_TYPE_CINEPAK,
	HI_UNF_VCODEC_TYPE_INDEO2,
	HI_UNF_VCODEC_TYPE_INDEO3,
	HI_UNF_VCODEC_TYPE_INDEO4,
	HI_UNF_VCODEC_TYPE_INDEO5,
	HI_UNF_VCODEC_TYPE_MJPEGB,
	HI_UNF_VCODEC_TYPE_MVC,
	HI_UNF_VCODEC_TYPE_HEVC,
	HI_UNF_VCODEC_TYPE_DV,
	HI_UNF_VCODEC_TYPE_VP9,
	HI_UNF_VCODEC_TYPE_BUTT
};

struct mpeg2_sequence;
class CBitstreamParser;
class CBitstreamConverter;

class CDVDVideoCodecHisilicon : public CDVDVideoCodec {
	public:
		CDVDVideoCodecHisilicon(CProcessInfo &processInfo) : CDVDVideoCodec(processInfo),
															 m_speed(DVD_PLAYSPEED_NORMAL),
															 m_opened(false),
															 m_last_pts(AV_NOPTS_VALUE),
															 m_framerate(0.0),
															 m_bitparser(NULL),
															 m_bitstream(NULL),
															 m_video_rate(0),
															 m_bufferIndex(0),
															 m_has_keyframe(false),
															 m_mpeg2_sequence(NULL),
															 m_codecControlFlags(0) {}
		virtual ~CDVDVideoCodecHisilicon() { Dispose(); }

		static CDVDVideoCodec* Create(CProcessInfo &processInfo);
		static bool Register();

		// Required overrides
		virtual bool Open(CDVDStreamInfo &hints, CDVDCodecOptions &options);
		virtual bool AddData(const DemuxPacket &packet) override;
		virtual void Reset(void);
		virtual VCReturn GetPicture(VideoPicture *pVideoPicture);
		virtual void SetSpeed(int iSpeed);
		virtual void SetCodecControl(int flags);
		virtual const char *GetName(void) { return (const char *)m_pFormatName.c_str(); }

	protected:
		void Dispose(void);
		void FrameRateTracking(uint8_t *pData, int iSize, double dts, double pts);
		void ClearBuffer(bool IsToClear);

		CDVDStreamInfo       m_hints;
		int                  m_video;
		bool                 m_drain;
		int                  m_speed;
		bool                 m_opened;
		int64_t              m_last_pts;
		double               m_framerate;
		CBitstreamParser    *m_bitparser;
		CBitstreamConverter *m_bitstream;
		int                  m_video_rate;
		uint64_t             m_bufferIndex;
		VideoPicture         m_videobuffer;
		std::string          m_pFormatName;
		bool                 m_has_keyframe;
		float                m_aspect_ratio;
		mpeg2_sequence      *m_mpeg2_sequence;
		int                  m_codecControlFlags;
		double               m_mpeg2_sequence_pts;

	private:
		static std::atomic<bool> m_InstanceGuard;
};

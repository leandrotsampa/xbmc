/*
 *      Copyright (C) 2005-2011 Team XBMC
 *      http://www.xbmc.org
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC; see the file COPYING.  If not, write to
 *  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 *  http://www.gnu.org/copyleft/gpl.html
 *
 */

#include <sys/ioctl.h>
#include <linux/dvb/video.h>
#include <linux/dvb/audio.h>

#include "DatabaseManager.h"
#include "DVDVideoCodecHisilicon.h"
#include "DVDClock.h"
#include "DVDStreamInfo.h"
#include "settings/AdvancedSettings.h"
#include "settings/Settings.h"
#include "settings/SettingsComponent.h"
#include "ServiceBroker.h"
#include "video/VideoThumbLoader.h"
#include "utils/log.h"
#include "utils/BitstreamConverter.h"
#include "utils/SysfsUtils.h"
#include "DVDCodecs/DVDFactoryCodec.h"

#define __MODULE_NAME__ "CDVDVideoCodecHisilicon"

CDVDVideoCodec* CDVDVideoCodecHisilicon::Create(CProcessInfo &processInfo) {
	return new CDVDVideoCodecHisilicon(processInfo);
}

bool CDVDVideoCodecHisilicon::Register() {
	CDVDFactoryCodec::RegisterHWVideoCodec("hisilicon_dec", &CDVDVideoCodecHisilicon::Create);
	return true;
}

std::atomic<bool> CDVDVideoCodecHisilicon::m_InstanceGuard(false);
bool CDVDVideoCodecHisilicon::Open(CDVDStreamInfo &hints, CDVDCodecOptions &options) {
	HI_UNF_VCODEC_TYPE_E streamtype = HI_UNF_VCODEC_TYPE_BUTT;
	m_hints = hints;

	if (!CServiceBroker::GetSettingsComponent()->GetSettings()->GetBool(CSettings::SETTING_VIDEOPLAYER_USEHICODEC))
		return false;
	else if (hints.stills || hints.width == 0)
		return false;

	// allow only 1 instance here
	if (m_InstanceGuard.exchange(true)) {
		CLog::Log(LOGERROR, "%s::%s -> InstanceGuard locked.", __MODULE_NAME__, __FUNCTION__);
		return false;
	}

	switch (m_hints.codec) {
		case AV_CODEC_ID_MPEG1VIDEO:
		case AV_CODEC_ID_MPEG2VIDEO:
			if (m_hints.height <= CServiceBroker::GetSettingsComponent()->GetSettings()->GetInt(CSettings::SETTING_VIDEOPLAYER_USEHICODECMPEG2))
				goto FAILED;

			streamtype = HI_UNF_VCODEC_TYPE_MPEG2;
			m_mpeg2_sequence_pts = 0;
			m_mpeg2_sequence = new mpeg2_sequence;
			m_mpeg2_sequence->width = m_hints.width;
			m_mpeg2_sequence->height = m_hints.height;
			m_mpeg2_sequence->ratio = m_hints.aspect;
			m_mpeg2_sequence->fps_rate = m_hints.fpsrate;
			m_mpeg2_sequence->fps_scale = m_hints.fpsscale;
		break;
		case AV_CODEC_ID_H264:
			if (m_hints.height <= CServiceBroker::GetSettingsComponent()->GetSettings()->GetInt(CSettings::SETTING_VIDEOPLAYER_USEHICODECH264))
				goto FAILED;

			streamtype = HI_UNF_VCODEC_TYPE_H264;
			if (m_hints.extradata && *(uint8_t*)m_hints.extradata == 1) {
				m_bitstream = new CBitstreamConverter;
				if (!m_bitstream->Open(m_hints.codec, (uint8_t*)m_hints.extradata, m_hints.extrasize, true))
					SAFE_DELETE(m_bitstream);
			} else {
				m_bitparser = new CBitstreamParser();
				m_bitparser->Open();
			}
		break;
		case AV_CODEC_ID_MPEG4:
		case AV_CODEC_ID_MSMPEG4V2:
		case AV_CODEC_ID_MSMPEG4V3:
			if (m_hints.height <= CServiceBroker::GetSettingsComponent()->GetSettings()->GetInt(CSettings::SETTING_VIDEOPLAYER_USEHICODECMPEG4))
				goto FAILED;

			streamtype = HI_UNF_VCODEC_TYPE_MPEG4;
		break;
		case AV_CODEC_ID_H263:
		case AV_CODEC_ID_H263P:
		case AV_CODEC_ID_H263I:
			streamtype = HI_UNF_VCODEC_TYPE_H263;
		break;
		case AV_CODEC_ID_RV10:
		case AV_CODEC_ID_RV20:
		case AV_CODEC_ID_RV30:
		case AV_CODEC_ID_RV40:
			streamtype = HI_UNF_VCODEC_TYPE_REAL9;
		break;
		case AV_CODEC_ID_VC1:
		case AV_CODEC_ID_FLV1:
		case AV_CODEC_ID_WMV3:
			streamtype = HI_UNF_VCODEC_TYPE_VC1;
		break;
		case AV_CODEC_ID_VP6:
			streamtype = HI_UNF_VCODEC_TYPE_VP6;
		break;
		case AV_CODEC_ID_VP6A:
			streamtype = HI_UNF_VCODEC_TYPE_VP6A;
		break;
		case AV_CODEC_ID_VP6F:
			streamtype = HI_UNF_VCODEC_TYPE_VP6F;
		break;
		case AV_CODEC_ID_VP8:
			streamtype = HI_UNF_VCODEC_TYPE_VP8;
		break;
		case AV_CODEC_ID_VP9:
			streamtype = HI_UNF_VCODEC_TYPE_VP9;
		break;
		case AV_CODEC_ID_HEVC:
			streamtype = HI_UNF_VCODEC_TYPE_HEVC;
			m_bitstream = new CBitstreamConverter();
			if (!m_bitstream->Open(m_hints.codec, (uint8_t*)m_hints.extradata, m_hints.extrasize, true))
				SAFE_DELETE(m_bitstream);
		break;
		default:
			CLog::Log(LOGERROR, "%s::%s -> The Codec %s(%d) Profile %s it's not supported.", __MODULE_NAME__, __FUNCTION__, avcodec_get_name(m_hints.codec), m_hints.codec, avcodec_profile_name(m_hints.codec, m_hints.profile));
			goto FAILED;
		break;
	}

	m_pFormatName = std::string("hi-") + avcodec_get_name(m_hints.codec);

	if ((m_video = open(VIDEODEV, O_RDWR | O_NONBLOCK)) < 0) {
		CLog::Log(LOGERROR, "%s::%s -> Video Device - failed", __MODULE_NAME__, __FUNCTION__);
		goto FAILED_LOG;
	}

	if (ioctl(m_video, VIDEO_SELECT_SOURCE, VIDEO_SOURCE_MEMORY) == -1) {
		CLog::Log(LOGERROR, "%s::%s -> Set Memory Mode - failed", __MODULE_NAME__, __FUNCTION__);
		goto FAILED_LOG;
	} else {
		CLog::Log(LOGDEBUG, "%s::%s -> Set Memory Mode - ok", __MODULE_NAME__, __FUNCTION__);
	}

	if (ioctl(m_video, VIDEO_FREEZE) == -1)
		CLog::Log(LOGERROR, "%s::%s -> Set PAUSE State - failed", __MODULE_NAME__, __FUNCTION__);
	else
		CLog::Log(LOGDEBUG, "%s::%s -> Set PAUSE State - ok", __MODULE_NAME__, __FUNCTION__);
/*
	if (ioctl(m_video, AUDIO_SET_AV_SYNC, 0) == -1)
		CLog::Log(LOGERROR, "%s::%s -> Set SYNC State - failed", __MODULE_NAME__, __FUNCTION__);
	else
		CLog::Log(LOGDEBUG, "%s::%s -> Set SYNC State - ok", __MODULE_NAME__, __FUNCTION__);
*/
	if (ioctl(m_video, VIDEO_SET_STREAMTYPE, streamtype + HI_UNF_VCODEC_TYPE_BUTT) == -1) {
		CLog::Log(LOGERROR, "%s::%s -> Set CODEC(%d) - failed", __MODULE_NAME__, __FUNCTION__, streamtype);
		goto FAILED_LOG;
	} else {
		CLog::Log(LOGDEBUG, "%s::%s -> Set CODEC(%d) - ok", __MODULE_NAME__, __FUNCTION__, streamtype);
	}

	if (ioctl(m_video, VIDEO_PLAY) == -1) {
		CLog::Log(LOGERROR, "%s::%s -> Set PLAY State - failed", __MODULE_NAME__, __FUNCTION__);
		goto FAILED_LOG;
	} else {
		CLog::Log(LOGDEBUG, "%s::%s -> Set PLAY State - ok", __MODULE_NAME__, __FUNCTION__);
	}

	// allocate a dummy VideoPicture buffer.
	m_videobuffer.Reset();

	m_videobuffer.iWidth  = m_hints.width;
	m_videobuffer.iHeight = m_hints.height;

	m_videobuffer.iDisplayWidth  = m_videobuffer.iWidth;
	m_videobuffer.iDisplayHeight = m_videobuffer.iHeight;
	if (m_hints.aspect > 0.0 && !m_hints.forced_aspect) {
		m_videobuffer.iDisplayWidth  = ((int)lrint(m_videobuffer.iHeight * m_hints.aspect)) & ~3;
		if (m_videobuffer.iDisplayWidth > m_videobuffer.iWidth) {
			m_videobuffer.iDisplayWidth  = m_videobuffer.iWidth;
			m_videobuffer.iDisplayHeight = ((int)lrint(m_videobuffer.iWidth / m_hints.aspect)) & ~3;
		}
	}

	m_processInfo.SetVideoDecoderName(m_pFormatName, true);
	m_processInfo.SetVideoPixelFormat("");
	m_processInfo.SetVideoDimensions(m_hints.width, m_hints.height);
	m_processInfo.SetVideoDeintMethod("none");
	m_processInfo.SetVideoDAR(m_hints.aspect);

	m_opened = true;
	m_has_keyframe = false;
	m_aspect_ratio = m_hints.aspect;
	ClearBuffer(true);
	SysfsUtils::SetString("/proc/stb/video/hdmi_hdrtype", "auto");

	CLog::Log(LOGINFO, "%s::%s -> Using Codec %s Profile %s Content Encrypted: %s.", __MODULE_NAME__, __FUNCTION__, m_pFormatName, avcodec_profile_name(m_hints.codec, m_hints.profile), m_hints.cryptoSession ? "Yes" : "No");
	return true;
FAILED_LOG:
	CLog::Log(LOGERROR, "%s::%s -> Failed to create Hisilicon Codec %s.", __MODULE_NAME__, __FUNCTION__, m_pFormatName);
FAILED:
	Dispose();
	return false;
}

bool CDVDVideoCodecHisilicon::AddData(const DemuxPacket &packet) {
	// Handle Input, add demuxer packet to input queue, we must accept it or
	// it will be discarded as VideoPlayerVideo has no concept of "try again".
	int iSize(packet.iSize);
	uint8_t *pData(packet.pData);
	int64_t vpts = AV_NOPTS_VALUE;
	int64_t avpts= AV_NOPTS_VALUE;
	int64_t avdts= AV_NOPTS_VALUE;

	if (pData) {
		if (m_bitstream) {
			if (!m_bitstream->Convert(pData, iSize))
				return true;

			if (!m_bitstream->CanStartDecode()) {
				CLog::Log(LOGDEBUG, "%s::%s -> waiting for keyframe (bitstream).", __MODULE_NAME__, __FUNCTION__);
				return true;
			}

			pData = m_bitstream->GetConvertBuffer();
			iSize = m_bitstream->GetConvertSize();
		} else if (!m_has_keyframe && m_bitparser) {
			if (!m_bitparser->CanStartDecode(pData, iSize)) {
				CLog::Log(LOGDEBUG, "%s::%s -> waiting for keyframe (bitparser).", __MODULE_NAME__, __FUNCTION__);
				return true;
			} else {
				m_has_keyframe = true;
			}
		}

		FrameRateTracking(pData, iSize, packet.dts, packet.pts);
	} else {
		return false;
	}

	if (m_hints.ptsinvalid || packet.pts == DVD_NOPTS_VALUE)
		avpts = AV_NOPTS_VALUE;
	else
		avpts = 0.5 + (packet.pts * PTS_FREQ) / DVD_TIME_BASE;

	if (packet.dts == DVD_NOPTS_VALUE) {
		avdts = avpts;
	} else {
		avdts = 0.5 + (packet.dts * PTS_FREQ) / DVD_TIME_BASE;

		// For VC1 AML decoder uses PTS only on I-Frames
		if ((m_hints.codec == AV_CODEC_ID_H263 || m_hints.codec == AV_CODEC_ID_MPEG4 || m_hints.codec == AV_CODEC_ID_H263P ||
			m_hints.codec == AV_CODEC_ID_H263I || m_hints.codec == AV_CODEC_ID_MSMPEG4V2 ||
			m_hints.codec == AV_CODEC_ID_MSMPEG4V3 || m_hints.codec == AV_CODEC_ID_FLV1 || /* MPEG4 */
			m_hints.codec == AV_CODEC_ID_WMV3 || m_hints.codec == AV_CODEC_ID_VC1) &&      /* VC1   */
			avpts == AV_NOPTS_VALUE)
			avpts = avdts;
	}

	if (avpts != AV_NOPTS_VALUE) {
		vpts = avpts;
	} else if (avdts != AV_NOPTS_VALUE) {
		vpts = avdts;
	} else {
		vpts = 0;
	}

	/* PTS Header "SPTS"
	 * This will be used by AVServer to save PTS and use when received ES Data.
	 * It's more easy than convert ES Data to PES Data.
	 *
	 * Codec works without this, but one or other videos have problema to sync.
	 */
	if (vpts != AV_NOPTS_VALUE) {
		char pts_header[9];
		pts_header[0] = 0x53;
		pts_header[1] = 0x50;
		pts_header[2] = 0x54;
		pts_header[3] = 0x53;
		pts_header[4] = 0x21 | ((vpts >> 29) & 0xE);
		pts_header[5] = vpts >> 22;
		pts_header[6] = 0x01 | ((vpts >> 14) & 0xFE);
		pts_header[7] = vpts >> 7;
		pts_header[8] = 0x01 | ((vpts << 1) & 0xFE);

		if (write(m_video, pts_header, 9) <= 0) {
			CLog::Log(LOGERROR, "%s::%s -> Failed to write PTS Header.", __MODULE_NAME__, __FUNCTION__);
			return false;
		}
	}

	return (write(m_video, pData, iSize) > 0);
}

void CDVDVideoCodecHisilicon::Reset(void) {
	CLog::Log(LOGDEBUG, "%s::%s -> called.", __MODULE_NAME__, __FUNCTION__);

	// reset some interal vars
	m_has_keyframe = false;
	m_mpeg2_sequence_pts = 0;
	m_last_pts= AV_NOPTS_VALUE;
	if (m_bitstream && m_hints.codec == AV_CODEC_ID_H264)
		m_bitstream->ResetStartDecode();
	SetSpeed(m_speed);
}

CDVDVideoCodec::VCReturn CDVDVideoCodecHisilicon::GetPicture(VideoPicture* pVideoPicture) {
	int64_t pts;

	pVideoPicture->SetParams(m_videobuffer);

	if ((ioctl(m_video, VIDEO_GET_PTS, &pts) != -1) && (pts != m_last_pts)) {
		pVideoPicture->dts = DVD_NOPTS_VALUE;
		pVideoPicture->pts = static_cast<double>(pts) / PTS_FREQ * DVD_TIME_BASE;
		m_last_pts = pts;
		m_bufferIndex++;
	} else if (m_drain) {
		return VC_EOF;
	} else {
		return VC_BUFFER;
	}

	// check for mpeg2 aspect ratio changes
	if (m_mpeg2_sequence && pVideoPicture->pts >= m_mpeg2_sequence_pts)
		m_aspect_ratio = m_mpeg2_sequence->ratio;

	pVideoPicture->iDisplayWidth  = pVideoPicture->iWidth;
	pVideoPicture->iDisplayHeight = pVideoPicture->iHeight;
	if (m_aspect_ratio > 1.0 && !m_hints.forced_aspect) {
		pVideoPicture->iDisplayWidth  = ((int)lrint(pVideoPicture->iHeight * m_aspect_ratio)) & ~3;
		if (pVideoPicture->iDisplayWidth > pVideoPicture->iWidth) {
			pVideoPicture->iDisplayWidth  = pVideoPicture->iWidth;
			pVideoPicture->iDisplayHeight = ((int)lrint(pVideoPicture->iWidth / m_aspect_ratio)) & ~3;
		}
	}

	return VC_PICTURE;
}

void CDVDVideoCodecHisilicon::SetCodecControl(int flags) {
	if (m_codecControlFlags != flags) {
		m_codecControlFlags = flags;

		if (flags & DVD_CODEC_CTRL_DROP)
			m_videobuffer.iFlags |= DVP_FLAG_DROPPED;
		else
			m_videobuffer.iFlags &= ~DVP_FLAG_DROPPED;

		m_drain = ((flags & DVD_CODEC_CTRL_DRAIN) != 0);
	}
}

void CDVDVideoCodecHisilicon::SetSpeed(int iSpeed) {
	CLog::Log(LOGDEBUG, "%s::%s -> Normal %d Speed %d m_speed %d", __MODULE_NAME__, __FUNCTION__, DVD_PLAYSPEED_NORMAL, iSpeed, m_speed);

	if (!m_opened)
		return;

	switch (iSpeed) {
		case DVD_PLAYSPEED_PAUSE:
			if (ioctl(m_video, VIDEO_FREEZE) == -1)
				CLog::Log(LOGERROR, "%s::%s -> Set PAUSE State - failed", __MODULE_NAME__, __FUNCTION__);
			else
				CLog::Log(LOGDEBUG, "%s::%s -> Set PAUSE State - ok", __MODULE_NAME__, __FUNCTION__);

			ClearBuffer((m_speed == DVD_PLAYSPEED_NORMAL && m_bufferIndex < 1) || // When video start or resumed from point.
						(m_speed == DVD_PLAYSPEED_PAUSE && m_bufferIndex > 1));   // When direct seek by pointer.
		break;
		case DVD_PLAYSPEED_NORMAL:
			if (m_speed != DVD_PLAYSPEED_NORMAL && m_speed != DVD_PLAYSPEED_PAUSE) {
				if (ioctl(m_video, VIDEO_FAST_FORWARD, 0) == -1)
					CLog::Log(LOGERROR, "%s::%s -> Restore Speed - failed", __MODULE_NAME__, __FUNCTION__);
				else
					CLog::Log(LOGDEBUG, "%s::%s -> Restore Speed - ok", __MODULE_NAME__, __FUNCTION__);

				ClearBuffer(m_speed > DVD_PLAYSPEED_NORMAL); // Backward not need clear buffer.
			} else {
				if (ioctl(m_video, VIDEO_CONTINUE) == -1)
					CLog::Log(LOGERROR, "%s::%s -> Set RESUME State - failed", __MODULE_NAME__, __FUNCTION__);
				else
					CLog::Log(LOGDEBUG, "%s::%s -> Set RESUME State - ok", __MODULE_NAME__, __FUNCTION__);
			}
		break;
		default:
			ClearBuffer(true);

			if (ioctl(m_video, VIDEO_FAST_FORWARD, iSpeed / DVD_PLAYSPEED_NORMAL) == -1)
				CLog::Log(LOGERROR, "%s::%s -> TrickPlay %d - failed", __MODULE_NAME__, __FUNCTION__, iSpeed / DVD_PLAYSPEED_NORMAL);
			else
				CLog::Log(LOGDEBUG, "%s::%s -> TrickPlay %d - ok", __MODULE_NAME__, __FUNCTION__, iSpeed / DVD_PLAYSPEED_NORMAL);
		break;
	}

	// update internal vars regardless
	// of if we are open or not.
	m_speed = iSpeed;
}

void CDVDVideoCodecHisilicon::Dispose(void) {
	if (m_video != -1) {
		if (ioctl(m_video, VIDEO_STOP) == -1)
			CLog::Log(LOGERROR, "%s::%s -> Set STOP State - failed", __MODULE_NAME__, __FUNCTION__);
		else
			CLog::Log(LOGDEBUG, "%s::%s -> Set STOP State - ok", __MODULE_NAME__, __FUNCTION__);

		ClearBuffer(true);
		ioctl(m_video, VIDEO_SLOWMOTION, 0);
		ioctl(m_video, VIDEO_FAST_FORWARD, 0);
		//ioctl(m_video, AUDIO_SET_AV_SYNC, 1);
		ioctl(m_video, VIDEO_SELECT_SOURCE, VIDEO_SOURCE_DEMUX);
		close(m_video), m_video = -1;
	}

	SAFE_DELETE(m_mpeg2_sequence);
	SAFE_DELETE(m_bitstream);
	SAFE_DELETE(m_bitparser);

	m_opened = false;
	m_videobuffer.iFlags = 0;
	m_InstanceGuard.exchange(false);
}

void CDVDVideoCodecHisilicon::FrameRateTracking(uint8_t *pData, int iSize, double dts, double pts) {
	// mpeg2 handling
	if (m_mpeg2_sequence) {
		// probe demux for sequence_header_code NAL and
		// decode aspect ratio and frame rate.
		if (CBitstreamConverter::mpeg2_sequence_header(pData, iSize, m_mpeg2_sequence) &&
			(m_mpeg2_sequence->fps_rate > 0) && (m_mpeg2_sequence->fps_scale > 0)) {
			m_mpeg2_sequence_pts = pts;
			if (m_mpeg2_sequence_pts == DVD_NOPTS_VALUE)
				m_mpeg2_sequence_pts = dts;

			m_hints.fpsrate = m_mpeg2_sequence->fps_rate;
			m_hints.fpsscale = m_mpeg2_sequence->fps_scale;
			m_framerate = static_cast<float>(m_mpeg2_sequence->fps_rate) / m_mpeg2_sequence->fps_scale;
			m_video_rate = (int)(0.5 + (96000.0 / m_framerate));

			m_hints.width    = m_mpeg2_sequence->width;
			m_hints.height   = m_mpeg2_sequence->height;
			m_hints.aspect   = m_mpeg2_sequence->ratio;

			m_processInfo.SetVideoFps(m_framerate);
			m_processInfo.SetVideoDAR(m_hints.aspect);
		}
	}
}

void CDVDVideoCodecHisilicon::ClearBuffer(bool IsToClear) {
	if (!m_opened || !IsToClear)
		return;

	if (ioctl(m_video, VIDEO_CLEAR_BUFFER) == -1)
		CLog::Log(LOGERROR, "%s::%s -> Clear Buffer (%lld) - failed", __MODULE_NAME__, __FUNCTION__, m_bufferIndex);
	else
		CLog::Log(LOGDEBUG, "%s::%s -> Clear Buffer (%lld) - ok", __MODULE_NAME__, __FUNCTION__, m_bufferIndex);
}

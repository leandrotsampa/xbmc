#.rst:
# FindAML
# -------
# Finds the AML codec
#
# This will define the following variables::
#
# AML_FOUND - system has AML
# AML_INCLUDE_DIRS - the AML include directory
# AML_DEFINITIONS - the AML definitions
#
# and the following imported targets::
#
#   AML::AML   - The AML codec

set(HISILICON_FOUND true)

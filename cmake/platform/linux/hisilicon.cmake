set(PLATFORM_REQUIRED_DEPS EGL Hisilicon LibInput Xkbcommon)
set(HISILICON_RENDER_SYSTEM "" CACHE STRING "Render system to use with Hisilicon: \"gl\" or \"gles\"")

if(HISILICON_RENDER_SYSTEM STREQUAL "gl")
  list(APPEND PLATFORM_REQUIRED_DEPS OpenGl)
  set(APP_RENDER_SYSTEM gl)
elseif(HISILICON_RENDER_SYSTEM STREQUAL "gles")
  list(APPEND PLATFORM_REQUIRED_DEPS OpenGLES)
  set(APP_RENDER_SYSTEM gles)
else()
  message(SEND_ERROR "You need to decide whether you want to use GL- or GLES-based rendering in combination with the Hisilicon windowing system. Please set HISILICON_RENDER_SYSTEM to either \"gl\" or \"gles\". For normal desktop systems, you will usually want to use \"gl\".")
endif()

list(APPEND PLATFORM_DEFINES -DMESA_EGL_NO_X11_HEADERS -DHAS_HICODEC=1 -DTARGET_HISILICON=1)